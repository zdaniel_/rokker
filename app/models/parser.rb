class Parser
  include ActiveModel::Model

  attr_reader :input, :raw_input

  def initialize(input = "")
    @input = input
    @raw_input = input.clone
    process
  end

  def process
    replace_brands
    replace_clothing_types
  end

  def replace_brands
    brands.each do |brand|
      expresion = create_regexp_word(brand, 'b')
      replacement = create_html_word(brand,'b')
      @raw_input.gsub!(expresion,replacement)
    end
  end

  def replace_clothing_types
    clothing_types.each do |clothing_type|
      expresion = create_regexp_word(clothing_type, 'i')
      replacement = create_html_word(clothing_type,'i')
      @raw_input.gsub!(expresion,replacement)
    end
  end

  private

  def create_regexp_word(name,tag)
    Regexp.new("(?!(<#{tag}>))?(?<=\s|^)#{name}(?=\s|$)(?!(<\/#{tag}>))","i")
  end

  def create_html_word(name, tag)
    "<#{tag}>#{name}</#{tag}>"
  end

  def replace_with_italic(name)
    "<i>#{name}</i>"
  end

  def replace_with_bold(name)
    "<b>#{name}</b>"
  end

  def clothing_types
    ClothingType.pluck(:name).sort { |name| name.split(' ').length }
  end

  def brands
    Brand.pluck(:name).sort { |name| name.split(' ').length }
  end

end
