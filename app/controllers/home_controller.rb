class HomeController < ApplicationController
  def index
    @parser = Parser.new
  end

  def search
    @parser = Parser.new(params_parse)
  end

  private

  def params_parse
    filtered = params.require(:parser).permit(:input)
    filtered[:input]
  end
end
