require 'rails_helper'

RSpec.describe Parser, type: :model do
  before do
    Brand.create(name: "Gap")
    Brand.create(name: "Banana Republic")
    Brand.create(name: "Boss")
    Brand.create(name: "Hugo Boss")
    Brand.create(name: "Taylor")
    Brand.create(name: "Rebecca Taylor")

    ClothingType.create(name: "Denim")
    ClothingType.create(name: "Pants")
    ClothingType.create(name: "Sweaters")
    ClothingType.create(name: "Skirts")
    ClothingType.create(name: "Dresses")
  end
  describe "#process" do

    let(:parser) { Parser.new(case_test) }
    subject { parser.raw_input }
    context 'Gap Floral Pants' do
      let(:case_test) { "Gap Floral Pants" }
      it { is_expected.to eql('<b>Gap</b> Floral <i>Pants</i>') }
    end

    context 'gap floral pants' do
      let(:case_test) { "gap floral pants" }
      it { is_expected.to eql('<b>Gap</b> floral <i>Pants</i>') }
    end

    context 'hugo boss floral pants' do
      let(:case_test) { "hugo boss floral pants" }
      it { is_expected.to eql('<b>Hugo Boss</b> floral <i>Pants</i>') }
    end

    context 'hugo hugo boss boss floral pants' do
      let(:case_test) { "hugo hugo boss boss floral pants" }
      it { is_expected.to eql('hugo <b>Hugo Boss</b> <b>Boss</b> floral <i>Pants</i>') }
    end

    context 'hugo hugo boss boss floral pantspants' do
      let(:case_test) { "hugo hugo boss boss floral pantspants" }
      it { is_expected.to eql('hugo <b>Hugo Boss</b> <b>Boss</b> floral pantspants') }
    end
  end
end
